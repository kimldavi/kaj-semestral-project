export class CardCreator {
    #canvas;
    #ctx;
    #cardFrame;
    #cardImage;

    #cameras;
    #cameraStreams;

    #imageSourceCheckbox;
    #downloadButton;
    #imageFileInput;
    #manaInput;
    #attackInput;
    #healthInput;
    #textInput;
    #nameInput;

    #currMana;
    #currAttack;
    #currHealth;
    #currText;
    #currName;

    constructor() {
        this.#canvas = document.querySelector('canvas');
        this.#ctx = this.#canvas.getContext('2d');

        this.#cardFrame = new Image();
        this.#cardFrame.src = './images/frame.png';
        this.#cardFrame.onload = this.drawFrameWithText.bind(this);

        this.#imageSourceCheckbox = document.querySelector('#image-source-checkbox-input');
        this.#imageSourceCheckbox.addEventListener('change', this);

        this.#imageFileInput = document.querySelector('#image-file-input');
        this.#imageFileInput.addEventListener('change', this);

        this.#downloadButton = document.querySelector('#download');
        this.#downloadButton.addEventListener('click', this);

        this.#manaInput = document.querySelector('#mana');
        this.#manaInput.addEventListener('change', this);
        this.#currMana = this.#manaInput.value;

        this.#attackInput = document.querySelector('#attack');
        this.#attackInput.addEventListener('change', this);
        this.#currAttack = this.#attackInput.value;

        this.#healthInput = document.querySelector('#health');
        this.#healthInput.addEventListener('change', this);
        this.#currHealth = this.#healthInput.value;

        this.#textInput = document.querySelector('#text');
        this.#textInput.addEventListener('input', this);
        this.#currText = this.mergeWords(this.#textInput);

        this.#nameInput = document.querySelector('#name');
        this.#nameInput.addEventListener('input', this);
        this.#currName = this.#nameInput.value;
        setTimeout(() => { this.#nameInput.focus() }, 0);

        this.#cameraStreams = [];
        this.#cameras = [];
    }

    drawFrameWithText() {
        this.cropImage();
        //Draw card image
        this.#ctx.drawImage(this.#cardFrame, 0, 0, this.#canvas.offsetWidth, this.#canvas.offsetHeight);

        this.#ctx.save();
        this.#ctx.font = 'bold 45px Arial';
        this.#ctx.textAlign = 'center';
        this.#ctx.fillStyle = 'white';
        this.#ctx.shadowOffsetX = 5;
        this.#ctx.shadowOffsetY = 3;
        this.#ctx.shadowBlur = 2;
        this.#ctx.shadowColor = 'black';

        // Draw the health, attack and mana
        this.#ctx.fillText(this.#currHealth, 617, 615);
        this.#ctx.fillText(this.#currAttack, 303, 610);
        this.#ctx.fillText(this.#currMana, 300, 125);
        this.#ctx.restore();

        this.#ctx.save();
        this.#ctx.font = '24px Arial';
        this.#ctx.fillStyle = 'black';
        this.#ctx.textAlign = 'center';
        this.#ctx.shadowOffsetX = 3;
        this.#ctx.shadowOffsetY = 2;
        this.#ctx.shadowBlur = 2;
        this.#ctx.shadowColor = 'gray';

        // Draw the text inside the card
        for (let i = 0; i < this.#currText.length; i++) {
            this.#ctx.fillText(this.#currText[i], 465, 460 + i * 30);
        }

        this.#ctx.font = 'bold 26px Arial';
        this.#ctx.shadowOffsetX = 3;
        this.#ctx.shadowOffsetY = 2;
        this.#ctx.shadowBlur = 5;
        this.#ctx.shadowColor = '#E1CA9F';
        // Draw name of the card
        this.#ctx.translate(452, 378);
        this.#ctx.rotate(-5 * Math.PI / 180);
        this.#ctx.fillText(this.#currName, 0, 0);
        this.#ctx.restore();
    }

    // use camera and draw it after load
    startCameraStream() {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(stream => {
                const video = document.createElement('video');
                video.srcObject = stream;
                video.onloadedmetadata = () => {
                    this.#cameras.push(stream);
                    video.play();
                    this.#cameraStreams.push(setInterval(() => {
                        this.drawFrameWithVideo(video);
                    }, 100));
                };
            })
            .catch(e => {
                alert(e);
                console.error('Could not access camera', e);
            });
    }

    // multiple streams can occur so stop them all
    stopCameraStream() {
        this.#cameras.forEach(camera => camera.getVideoTracks().forEach(stream => stream.stop()));
        this.#cameraStreams.forEach(clearInterval);
        this.#ctx.clearRect(0, 0, this.#canvas.offsetWidth, this.#canvas.offsetHeight);
        this.drawFrameWithText();
    }

    drawFrameWithVideo(video) {
        this.#ctx.drawImage(video, 340, 66, 235, 282);
        this.drawFrameWithText();
    }

    cropImage() {
        // Crop camera to fit the frame
        this.#ctx.save();
        this.#ctx.rotate(-25 * Math.PI / 180);
        this.#ctx.clearRect(120, 90, 290, 150);
        this.#ctx.restore();
        this.#ctx.save();
        this.#ctx.rotate(25 * Math.PI / 180);
        this.#ctx.clearRect(300, -300, 290, 150);
        this.#ctx.restore();
    }

    handleEvent(e) {
        // download image
        if (e.target == this.#downloadButton) {
            const dataURL = canvas.toDataURL('image/png');
            const link = document.createElement('a');
            link.href = dataURL;
            link.download = 'card.png';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
        // switch image source
        else if (e.target == this.#imageSourceCheckbox) {
            if (this.#imageSourceCheckbox.checked) {
                this.startCameraStream();
            } else {
                this.#imageFileInput.value = null;
                this.stopCameraStream();
            }
        }
        // set values
        else if (e.target == this.#manaInput && this.checkValue(this.#manaInput)) {
            this.#currMana = this.#manaInput.value;
            this.drawFrameWithText();
        }
        else if (e.target == this.#attackInput && this.checkValue(this.#attackInput)) {
            this.#currAttack = this.#attackInput.value;
            this.drawFrameWithText();
        }
        else if (e.target == this.#healthInput && this.checkValue(this.#healthInput)) {
            this.#currHealth = this.#healthInput.value;
            this.drawFrameWithText();
        }
        else if (e.target == this.#textInput) {
            this.#currText = this.mergeWords(this.#textInput);
            this.drawFrameWithText();
        }
        else if (e.target == this.#nameInput) {
            this.#currName = this.#nameInput.value;
            this.drawFrameWithText();
        }
        // read uploaded image and draw it
        else if (e.target == this.#imageFileInput) {
            const file = this.#imageFileInput.files[0];
            const fr = new FileReader();
            fr.addEventListener("load", e => {
                this.#cardImage = new Image();
                this.#cardImage.src = fr.result;
                this.#cardImage.onload = () => {
                    // Draw the image onto the canvas
                    this.#ctx.clearRect(0, 0, this.#canvas.offsetWidth, this.#canvas.offsetHeight);
                    this.#ctx.drawImage(this.#cardImage, 340, 66, 235, 350);
                    this.drawFrameWithText();
                };
            })
            fr.readAsDataURL(file);
        }
    }

    checkValue(input) {
        if (input.value < 0 || input.value > 999) {
            input.parentElement.classList.add('invalid-number');
            return false;
        }
        else {
            input.parentElement.classList.remove('invalid-number');
            return true;
        }
    }

    mergeWords(input) {
        const charsOnRow = 18;
        let textRows = [];
        let textWords = input.value.split(' ').filter(x => x != '');
        for (let i = 0; i < textWords.length; i++) {
            let currWord = textWords[i];
            if (currWord.length > 18) {
                input.parentElement.classList.add('invalid-text');
                return ["Too long word"];
            }
            while (currWord.length < charsOnRow && i < textWords.length - 1) {
                let nextWord = textWords[i + 1];
                if (currWord.length + 1 + nextWord.length <= charsOnRow) {
                    currWord += ' ' + nextWord;
                    i++;
                } else {
                    break;
                }
            }
            textRows.push(currWord);
        }
        if (textRows.length > 5) {
            input.parentElement.classList.add('invalid-text');
            return ["Text is too long"];
        }
        else {
            input.parentElement.classList.remove('invalid-text');
            return textRows;
        }
    }

    removeEventListeners() {
        this.#imageFileInput.removeEventListener('change', this);
        this.#nameInput.removeEventListener('input', this);
        this.#textInput.removeEventListener('input', this);
        this.#manaInput.removeEventListener('click', this);
        this.#healthInput.removeEventListener('click', this);
        this.#attackInput.removeEventListener('click', this);
        this.#imageSourceCheckbox.removeEventListener('change', this);
        this.#downloadButton.removeEventListener('click', this);
    }

}
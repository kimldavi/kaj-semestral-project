async function getCards(page, filters) {
    // if offline 
    if (!navigator.onLine) return [];
    try {
        document.body.classList.add('cards-loading')
        if (page < 1 || page > 39) return [];
        const token = await getToken();
        // 382 karet
        let cards = await fetch(
            `https://eu.api.blizzard.com/hearthstone/cards?page=${page}&pageSize=20&set=classic-cards&class=${filters.class}&rarity=${filters.rarity}&manaCost=${filters.mana}`,
            {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
            })
            .then(x => { return x.json() })
            .then(x => x.cards);
        return cards;
    } catch (e) {
        return [];
    } finally {
        document.body.classList.remove('cards-loading');
    }
};

async function getToken() {
    // token needed for api authorization
    try {
        return fetch('https://oauth.battle.net/token?grant_type=client_credentials',
            {
                headers: {
                    Authorization: 'Basic ZThiY2Y2YTNhZTBhNDg4MDgwNTQ1ZDFmZjU2NDg1ODU6UW53TzlVOHk3d3Uzd2RnSXM3VUp1Tnh6dEVhS01EalE='
                },
                method: "POST"
            })
            .then(response => response.json())
            .then(response => { return response.access_token });
    } catch (e) {
        alert("Blizard API error: " + e);
    }
};

export { getCards }
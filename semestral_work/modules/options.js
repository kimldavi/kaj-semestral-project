// draw options for filtering
export class Options {
    constructor() {
        this.addClassOptions();
        this.addManaOptions();
        this.addRarityOptions();
    }

    addClassOptions() {
        let classes = ["All", "Priest", "Hunter", "Druid",
            "Shaman", "Rogue", "Paladin",
            "Warlock", "Mage", "Warrior"];
        let select = document.querySelector("#cardClass");
        select.append(...classes.map(cardClass => {
            let option = document.createElement("option");
            option.value = cardClass.toLocaleLowerCase();
            option.textContent = cardClass;
            if (option.value == 'all')
                option.selected = true;
            return option;
        }));
    }

    addManaOptions() {
        let select = document.querySelector("#mana");

        let option = document.createElement("option");
        option.text = "All";
        option.value = "1,2,3,4,5,6,7,8,9";
        option.selected = true;

        select.append(option)
        for (let i = 1; i <= 9; i++) {
            let option = document.createElement("option");
            option.text = i;
            option.value = i;
            select.append(option);
        }
    }

    addRarityOptions() {
        let rarities = ["Common", "Rare", "Epic", "Legendary", "All"];
        let select = document.querySelector("#rarity");
        select.append(...rarities.map(rarity => {
            let option = document.createElement("option");
            option.value = rarity.toLocaleLowerCase();
            option.textContent = rarity;
            if (option.value == 'all')
                option.selected = true;
            return option;
        }));
    }
}
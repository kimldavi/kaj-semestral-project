export class Deck {
    #deckNumber;
    #cards;
    #legendary;

    constructor(number) {
        this.#deckNumber = number;
        this.#legendary = 5;

        let storedDeck = JSON.parse(localStorage.getItem('deck-' + this.#deckNumber));
        if (!storedDeck) {
            localStorage.setItem('deck-' + this.#deckNumber, JSON.stringify({ cards: [] }));
            this.#cards = [];
        }
        else this.#cards = storedDeck.cards;
    }

    getDeckNumber(){
        return this.#deckNumber;
    }

    addCard(card) {
        if (this.canAdd(card) && this.#cards.length < 30) {
            this.#cards.push(card);
            this.saveCardsToStorage();
        }
    }

    getCards() {
        return this.#cards.sort((card1, card2) =>
            card1.name.en_US.localeCompare(card2.name.en_US));
    }

    getCardsWithCount() {
        return Object.values(
            this.#cards.reduce((counts, card) => {
                if (!counts[card.id]) {
                    card.count = 1;
                    counts[card.id] = card;
                } else {
                    counts[card.id].count++;
                }
                return counts;
            }, {})
        )
    }

    removeCard(card) {
        let cardIdx = this.#cards.findIndex(x => x.id == card.id);
        this.#cards.splice(cardIdx, 1);
        this.saveCardsToStorage();
    }

    clearDeck(){
        this.#cards = [];
        this.saveCardsToStorage();   
    }

    canAdd(card) {
        return ((card.rarityId != this.#legendary && this.#cards.filter(x => x.id == card.id).length < 2))
            || (card.rarityId == this.#legendary && this.#cards.filter(x => x.id == card.id).length < 1);
    }

    saveCardsToStorage() {
        let storedDeck = JSON.parse(localStorage.getItem('deck-' + this.#deckNumber));
        storedDeck.cards = this.#cards;
        localStorage.setItem('deck-' + this.#deckNumber, JSON.stringify(storedDeck));
    }
}
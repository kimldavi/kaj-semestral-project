import { Page } from "./page.js";
import { Options } from '../options.js';
import { DeckBuilder } from '../deckBuilder.js'

export class PageDeckBuilder extends Page {
    constructor(settings) {
        super(settings);
    }

    render() {
        return `
        <div class="deck-builder">
        <div class="cards">
            <div class="filters-base">
                <div class="filters">
                    <form>
                        <div>
                            <div class="lst">
                                <label for="mana">Mana:</label>
                                <select name="mana" id="mana"></select>
                            </div>
                            <div class="lst">
                                <label for="cardClass">Class:</label>
                                <select name="cardClass" id="cardClass"></select>
                            </div>
                            <div class="lst">
                                <label for="rarity">Rarity:</label>
                                <select name="rarity" id="rarity"></select>
                            </div>
                        </div>
                        <button type="submit" class="filter">Filter</button>
                    </form>
                    <button class="hide-filter-button">X</button>
                </div>
                <button class="show-filter-button">Show Filters</button>
            </div>
            <div class="cards-container"></div>
            <nav class="pages">
                <button class="prev">Previous</button>
                <button class="next">Next</button>
            </nav>
        </div>
        <aside class="deck">
            <div class="deck-select"></div>
            <div class="deck-container"></div>
            <button class="clear-deck">Clear deck</button>
        </aside>
        </div>`;
    }

    pageShow() {
        super.pageShow();
        new Options();
        this.currClass = new DeckBuilder();
    }

    pageHide() {
        super.pageHide();
        this.currClass.removeEventListeners();
        document.body.classList.remove('cards-loading');
    }
}
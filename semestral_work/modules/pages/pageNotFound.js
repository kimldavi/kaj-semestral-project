import { Page } from "./page.js";

export class PageNotFound extends Page {
    render() {
        return `
						<h2>Not found</h2>
					`;
    }
}
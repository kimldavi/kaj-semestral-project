export class Router {
    constructor({ pages, defaultPage }) {
        this.pages = pages;
        this.defaultPage = defaultPage;
        this.currentPage = null;

        // first run
        this.route(window.location.href);

        // listen on url changes from user clicking back button
        window.addEventListener('popstate', e => {
            this.route(window.location.href);
        });

        const links = document.querySelectorAll('.page-selection');
        links.forEach(link => {
            link.addEventListener('click', e => {
                const element = e.target;
                if (element.nodeName === 'A') {
                    e.preventDefault();
                    this.route(element.href);
                    window.history.pushState(null, null, element.href);
                }
            });
        })

    }

    route(urlString) {
        const url = new URL(urlString);
        const page = url.searchParams.get('page');
        if (this.currentPage) {
            this.currentPage.pageHide();
        }

        const page404 = this.pages.find(p => p.key === '404');
        const pageInstanceMatched = this.pages.find(p => p.key === (page ?? this.defaultPage));
        const currentPage = pageInstanceMatched ?? page404;

        this.currentPage = currentPage;
        this.currentPage.pageShow();
    }
}
export class Page {
    constructor({ key, title }) {
        this.pageElement = document.querySelector(`main`);
        this.title = title;
        this.key = key;
    }

    render() {
        return ``;
    }

    pageShow() {
        this.pageElement.innerHTML = this.render();
        document.title = this.title;
        const pageName = document.querySelector('#page-name');
        pageName.textContent = this.title;
    }

    pageHide() {
        this.pageElement.innerHTML = '';
    }
}
import { Page } from "./page.js";
import {CardCreator} from '../cardCreator.js'

export class PageCardCreator extends Page {
    constructor(settings) {
        super(settings);
    }

    render() {
        return `<div class="card-creator">
        <div class="create-card-container">
            <canvas width="900" height="700" id="canvas"></canvas>
        </div>
        <aside class="value-selection">
            <div class="source-selection">
                <label for="image-source-checkbox-input">Image</label>
                <input type="checkbox" id="image-source-checkbox-input" />
                <label for="image-source-checkbox-input" id="image-source-checkbox"></label>
                <label for="image-source-checkbox-input">Camera</label>
                <input type="file" id="image-file-input">
            </div>
            <div class="name-selection">
                <label for="name">Name:</label>
                <input type="text" id="name" maxlength="20" placeholder="Card name..." />
            </div>
            <div class="mana-selection">
                <label for="mana">Mana:</label>
                <input type="number" id="mana" value="9" min="1" max="999"/>
            </div>
            <div class="attack-selection">
                <label for="attack">Attack:</label>
                <input type="number" id="attack" value="6" min="1" max="999"/>
            </div>
            <div class="health-selection">
                <label for="health">Health:</label>
                <input type="number" id="health" value="6" min="1" max="999"/>
            </div>
            <div class="text-selection">
                <label for="text">Text:</label>
                <textarea id="text" placeholder="Card description..."></textarea>
            </div>
            <button id="download">Download</button>
        </aside>
    </div>`;
    }

    pageShow() {
        super.pageShow();
        this.currClass = new CardCreator();
    }

    pageHide() {
        this.currClass.stopCameraStream();
        this.currClass.removeEventListeners();
        super.pageHide();
    }
}
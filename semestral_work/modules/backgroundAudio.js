export class BackgroundAudio {
  #audio;
  #svg;
  #path;
  #playing;

  constructor() {
    this.#playing = false;
    this.#audio = new Audio();
    this.#audio.src = "sounds/hs.mp3";
    this.#audio.loop = true;

    this.#svg = document.querySelector('#svg-music');
    this.#svg.addEventListener("click", this);

    this.#path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    this.#path.setAttributeNS(null, 'd', 'M 10 0 L 50 50');
    this.#path.setAttributeNS(null, 'stroke', '#be0000');
    this.#path.setAttributeNS(null, 'stroke-width', '6px');
    this.#svg.append(this.#path);
  }

  play() {
    this.#path.remove();
    this.#audio.play();
  }

  pause() {
    this.#svg.append(this.#path);
    this.#audio.pause();
  }

  handleEvent(e) {
    if (!this.#playing) {
      this.play();
    }
    else {
      this.pause();
    }
    this.#playing = !this.#playing;
    e.stopPropagation();
  }
}
function drawDeck(deck) {
    let deckContainer = document.querySelector('.deck-container');
    if (!deckContainer) return;
    deckContainer.innerHTML = '';
    const p = document.createElement('p');
    p.innerText= 'Cards - ' + deck.getCards().length + '/30';
    deckContainer.append(p); 

    // need to get card counts
    const cardsWithCount = deck.getCardsWithCount();

    cardsWithCount.forEach(card => {
        let div = document.createElement('div');
        div.classList.add("card-in-deck");
        let divp = document.createElement('div');
        let p = document.createElement('p');
        p.textContent = card.count + "x " + card.name.en_US;
        let img = document.createElement('img');
        img.src = card.cropImage;
        img.alt = "card_image";
        img.classList.add("card-in-deck-image");;

        // add event to remove card from deck
        div.addEventListener('contextmenu', (e) => {
            e.preventDefault();
            deck.removeCard(card);
            let cardInList = document.querySelector(`#id${card.id}`);
            if (deck.canAdd(card) && cardInList) {
                cardInList.classList.remove('cannot-add');
            }
            drawDeck(deck);
        });
        divp.append(p);
        div.append(img, divp);
        deckContainer.append(div);
    });
}

function drawCards(cards, deck) {
    let div = document.querySelector(".cards-container");
    if (!div) return;
    div.innerHTML = '';
    cards.forEach(card => {
        let image = document.createElement("img");
        image.src = card.image.en_US;
        image.alt = "card_image";
        image.id = 'id' + card.id;
        image.draggable = true;
        image.classList.add("card");
        if (!deck.canAdd(card))
            setTimeout(() => image.classList.add('cannot-add'), 0);

        // add event for adding to deck
        image.addEventListener('click', () => {
            deck.addCard(card);
            if (!deck.canAdd(card))
                setTimeout(() => image.classList.add('cannot-add'), 0);
            drawDeck(deck);
        });
        div.append(image);
    });
}


export { drawCards, drawDeck }
import { Deck } from "./deck.js";
import * as blizzardApi from './blizzardApi.js'
import * as draw from './draw.js'

export class DeckBuilder {
    #showFilters;
    #currentFilters;
    #cardPage;
    #decks;
    #currDeck;
    #currCards;

    #prevButton;
    #nextButton;
    #showFilterButton;
    #hideFilterButton;
    #filterButton;
    #clearDeckButton;

    constructor() {
        this.#currentFilters = {
            mana: "1,2,3,4,5,6,7,8,9",
            class: "all",
            rarity: "all"
        };
        this.#showFilters = false;
        this.#cardPage = 1;

        this.#decks = [
            new Deck(3),
            new Deck(2),
            new Deck(1)
        ];
        this.createDecks();
        this.#currCards = this.loadCards();

        this.#nextButton = document.querySelector(".next");
        this.#prevButton = document.querySelector(".prev");
        this.#showFilterButton = document.querySelector('.show-filter-button');
        this.#hideFilterButton = document.querySelector('.hide-filter-button');
        this.#filterButton = document.querySelector(".filters");
        this.#clearDeckButton = document.querySelector(".clear-deck");

        this.#nextButton.addEventListener("click", this);
        this.#prevButton.addEventListener("click", this);
        this.#showFilterButton.addEventListener('click', this);
        this.#hideFilterButton.addEventListener('click', this);
        this.#filterButton.addEventListener('submit', this);
        this.#clearDeckButton.addEventListener('click', this);
        
        window.addEventListener("online", this);
    }

    createDecks() {
        let div = document.querySelector('.deck-select');
        this.#decks.forEach(deck => {
            const button = document.createElement('button');
            button.textContent = 'Deck ' + deck.getDeckNumber();

            // First is selected by default
            if (deck.getDeckNumber() == 1)
                button.classList.add('selected-deck');

            button.addEventListener('click', () => {
                let prevDeck = document.querySelector('.selected-deck');
                prevDeck.classList.remove('selected-deck');
                button.classList.add('selected-deck');
                this.#currDeck = deck;
                draw.drawDeck(this.#currDeck);
                this.#currCards = this.loadCards();
            })
            div.prepend(button);
        })
        this.#currDeck = this.#decks.find(deck => deck.getDeckNumber() == 1);
        draw.drawDeck(this.#currDeck);
    }

    loadCards() {
        blizzardApi.getCards(this.#cardPage, this.#currentFilters).then(cards => {
            draw.drawCards(cards, this.#currDeck);
        });
    }

    handleEvent(e) {
        // page stich
        if (e.target == this.#prevButton) {
            if (this.#cardPage <= 1) return;
            this.#cardPage--;
            this.loadCards();
        }
        else if (e.target == this.#nextButton) {
            this.#cardPage++;
            this.loadCards();
        }
        // filter hiding
        else if (e.target == this.#showFilterButton) {
            this.#showFilters = !this.#showFilters;
            if (this.#showFilters)
                document.body.classList.add('filter-show');
        }
        else if (e.target == this.#hideFilterButton) {
            this.#showFilters = !this.#showFilters;
            if (!this.#showFilters)
                document.body.classList.remove('filter-show');
        }
        // deck clearing
        else if (e.target == this.#clearDeckButton) {
            this.#currDeck.clearDeck();
            draw.drawDeck(this.#currDeck);
            this.loadCards();
        }
        else if (e.type == 'online') {
            this.loadCards();
        }
        // filter applying
        else {
            e.preventDefault();
            this.#cardPage = 1;
            const mana = document.querySelector("#mana");
            const cardClass = document.querySelector("#cardClass");
            const rarity = document.querySelector("#rarity");
            this.#currentFilters.mana = mana.value;
            this.#currentFilters.class = cardClass.value;
            this.#currentFilters.rarity = rarity.value;
            this.loadCards();
        }
    }

    removeEventListeners() {
        this.#nextButton.removeEventListener("click", this);
        this.#prevButton.removeEventListener("click", this);
        this.#showFilterButton.removeEventListener('click', this);
        this.#hideFilterButton.removeEventListener('click', this);
        this.#filterButton.removeEventListener('submit', this);
        this.#clearDeckButton.removeEventListener('click', this);
        window.removeEventListener('online', this);
    }

}

import { Router  } from './modules/pages/router.js';
import { PageCardCreator } from './modules/pages/pageCardCreator.js'
import { PageDeckBuilder } from './modules/pages/pageDeckBuilder.js'
import { PageNotFound } from './modules/pages/pageNotFound.js'
import { BackgroundAudio } from './modules/backgroundAudio.js';


new Router({
    pages: [
        new PageCardCreator({ key: 'card', title: 'Card Creator' }),
        new PageDeckBuilder({ key: 'deck', title: 'Deck Builder' }),
        new PageNotFound({ key: '404', title: 'Page not found' })
    ],
    defaultPage: 'deck'
});

new BackgroundAudio();



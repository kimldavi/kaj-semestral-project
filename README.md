# **KAJ semestrální práce** - David Kiml

## Téma - Aplikace pro stavění balíčků karet ze hry Hearthstone a tvorba vlastních karet

Tato aplikace je vytvořena jako single page application, která je složena primárně ze dvou funkcionalit:

##### **1) Stavění balíčků karet**

Uživateli jsou zobrazeny karty ze hry Hearthstone z veřejně dostupného Blizzard API. Tyto karty je možné filtrovat podle atributů. Po kliknutí na kartu se přidá do aktuálního balíčku. Pravým klikem na kartu v balíčku se z něj zase odebere. Jsou k dispozici 3  dostupné balíčky, které je možné upravovat. Balíček může obsahovat maximálně 30 karet, maximálně 2x stejnou kartu (v případě legendární karty jen jednu kopii).

##### **2) Vytváření vlastních karet**

Uživatel si může vytvořit vlastní kartu, kde může nastavit její atributy nebo název karty. Je možné přidat do karty obrázek, který uživatel nahraje, nebo může využít kameru zařízení. Výslednou kartu si pak může stáhnout do svého zařízení jako obrázek PNG.

---

Aplikace je také dostupná na stránce: http://kaj-semestral.funsite.cz/.
Stránka ale není zabezpečena a tak není možné v prohlížečích povolit kameru. Funkcionalita vykreslení kamery do karty není zde tedy dostupná. 

Pro spuštění aplikace na lokálním PC je potřeba mít webserver (například live server extension ve VS code). 
Načítání karet z Blizzard API občas trvá delší dobu. Může pomoci reload stránky.


| Požadavky          | Použití |
|-----------------------|------------|
| OOP přístup	prototypová dědičnost, její využití, jmenné prostory |  Kód javascriptu je rozdělen do tříd. Dědičnost je použita u tříd jednotlivých stránek |
| Použití pokročilých JS API  | File API - nahrání obrázku, LocalStorage - karty v balíku jsou uloženy, síťová komunikace s Blizzard API, UserMedia při použití kamery  |
| Funkční historie    | Při přepínání stránek se ukládají do historie a lze na ně jít zpět pomocí prohlížeče        |
| Ovládání medií | Video je streamováno do canvasu. Hudba v pozadí jde vypnout a zapnout          |
| Offline aplikace	       | Při offline statusu se nenačtou karty z API a obrázky. Po připojení by se měly načíst (v chromu mi navigator.onLine vždy vracel true i když to nebyla pravda)          |
| JS práce se SVG	 | Při kliknutí na obrázek noty se zašrtne a přestane hrát. V opačném případě zas hudba hrát začne       |





